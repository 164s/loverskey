#!/bin/bash

# Display Copyright notice
show_copyright() {
  echo "Copyright © 2014-2023 ALICE.REST All Rights Reserved."
  sleep 5
}

# Check user
check_user() {
  if [ "$(whoami)" != "loverskey" ]; then
    echo "You are not the loverskey user. Switching to the loverskey user."
    sudo -u loverskey -i
  fi
}

# Check and move directory
check_and_move_directory() {
  if [ "$(pwd)" != "/home/loverskey/loverskey" ]; then
    echo "The current directory is incorrect. Changing directory."
    cd /home/loverskey/loverskey
  fi
}

# Confirm update
confirm_update() {
  read -p "This is an update script for developers. Do not do this normally. From now on, it's your own responsibility. Is that OK? (y/n): " choice
  if [ "$choice" != "y" ]; then
    echo "Update aborted."
    exit 0
  fi
}

# Main script execution

show_copyright

check_user

check_and_move_directory

confirm_update

# Git pull and checkout the latest tag
git checkout -f develop
git pull

# Install packages and build
NODE_ENV=production pnpm install --frozen-lockfile
pnpm run clean
NODE_ENV=production pnpm run build
pnpm run migrate

echo "Update completed. Please restart the service with root privileges."
echo "sudo systemctl restart loverskey.service"
