<div align="center">
<a href="https://misskey-hub.net">
	<img src="./packages/frontend/assets/loverskey.png" alt=" ロゴ" width="400"/>
</a>

**🌎 **[Loverskey](https://misskey-hub.net/)** は、永遠に無料のオープンソースで分散型のソーシャルメディアプラットフォームです！ 🚀**

---

<a href="https://lovers.164.rest/">
		<img src="https://custom-icon-badges.herokuapp.com/badge/find_an-instance-acea31?logoColor=acea31&style=for-the-badge&logo=misskey&labelColor=363B40" alt="インスタンスを探す"/></a>

<a href="https://misskey-hub.net/docs/install.html">
		<img src="https://custom-icon-badges.herokuapp.com/badge/create_an-instance-FBD53C?logoColor=FBD53C&style=for-the-badge&logo=server&labelColor=363B40" alt="インスタンスを作成する"/></a>
---

<div>

<a href="https://xn--931a.moe/"><img src="https://github.com/kokonect-link/cherrypick/blob/develop/assets/ai.png?raw=true" align="right" height="320px"/></a>

## ✨ 特徴
- **ActivityPub サポート**\
Loverskeyにいなくても問題ありません！ Loverskeyインスタンスはお互いにコミュニケーションできるだけでなく、MastodonやMisskey、Pixelfedのような他のネットワークの人々とも友達になることができます！
- **リアクション**\
どの投稿にも絵文字のリアクションを追加できます！いいねボタンに縛られることはもうありません。ボタンをタップするだけで、自分の気持ちを正確に表現できます。
- **ドライブ**\
Loverskeyの組み込みドライブを使用すると、ソーシャルメディア内でクラウドストレージを提供し、任意のファイルをアップロードし、フォルダを作成し、自分の投稿からメディアを見つけることができます！
- **豊富なWeb UI**\
Loverskeyには豊富で使いやすいWeb UIがあります！
レイアウトを変更し、ウィジェットを追加してカスタマイズすることができ、独自のプログラミング言語であるAiScriptを使用してプラグインを作成することもできます。
- さらに多くの機能...

</div>

<div style="clear: both;"></div>

## ドキュメンテーション(Misskey)

Misskeyのドキュメンテーションは[Misskey Hub](https://misskey-hub.net/)で入手できます。上記のリンクとグラフィックの一部もそれぞれ特定のセクションにリンクしています。

## スポンサー

<div align="center">
	<a class="rss3" title="RSS3" href="https://rss3.io/" target="_blank"><img src="https://rss3.mypinata.cloud/ipfs/QmUG6H3Z7D5P511shn7sB4CPmpjH5uZWu4m5mWX7U3Gqbu" alt="RSS3" height="60"></a>
</div>

## 謝辞(Misskey)

<a href="https://www.chromatic.com/"><img src="https://user-images.githubusercontent.com/321738/84662277-e3db4f80-af1b-11ea-88f5-91d67a5e59f6.png" height="30" alt="Chromatic" /></a>

UIの変更をレビューし、ビジュアルのリグレッションをキャッチするのに役立つ視覚テストプラットフォームを提供している[Chromatic](https://www.chromatic.com/)に感謝します。

<a href="https://about.codecov.io/for/open-source/"><img src="https://about.codecov.io/wp-content/themes/codecov/assets/brand/sentry-cobranding/logos/codecov-by-sentry-logo.svg" height="30" alt="Codecov" /></a>

テストカバレッジを向上させるのに役立つコードカバレッジプラットフォームを提供している[Codecov](https://about.codecov.io/for/open-source/)に感謝します。

<a href="https://crowdin.com/"><img src="https://user-images.githubusercontent.com/20679825/230709597-1299a011-171a-4294-a91e-355a9b37c672.svg" height="30" alt="Crowdin" /></a>

多言語への翻訳をサポートしている[Misskeyを多言語に翻訳するためにCrowdin](https://crowdin.com/)に感謝します。

<a href="https://hub.docker.com/"><img src="https://user-images.githubusercontent.com/20679825/230148221-f8e73a32-a49b-47c3-9029-9a15c3824f92.png" height="30" alt="Docker" /></a>

Misskeyを本番環境で実行するのに役立つコンテナプラットフォームを提供している[Docker](https://hub.docker.com/)に感謝します。
